<h1>Portfolio Roelof Roessingh</h1>
<p>Portfolio with examples of projects;<br> 
-descriptions of completed projects<br>
-examples of documentation<br>
-examples of code: bash and python<br>

The code of each project is available for inspection, however, each project is also accompanied by visual guide that illustrates the functionality of the code since code execution sometimes relies on specific requirements and might be difficult to run straight away

If you have any questions, please contact me through LinkedIn:
https://www.linkedin.com/in/roelof-roessingh-1673a32b/

or email me at: 
roelof.roessingh@axxius.nl
</p>