#!/bin/bash

#todo;
#connect exit codes to readable descriptions
#auto log location to user home folder
#think about clean up of log files: delete after incubation period?

###SCRIPT STRUCTURE:
#EXIT CODE LEGEND
#VARIABLES
#SETTING CONFIGURATION
#GET USER INPUT
#IF ELSE BLOCK: tomcat or apache
#DATA LOGGING
#CLEANUP
#
#=============#=============#=============#=============
#EXIT CODE LEGEND
#0 =success 
#1 =log file already exists, manually aborted by user
#2 =user not root, not permitted to install  #not used atm
#3 =too many tries user input apache/tomcat
#4 =incorrect user input
#5 =user aborted process
#=============#=============#=============#=============
#VARIABLES:
log_path='/home/test/bash/logs/' #GENERAL LOG FILE LOCATION

if [ -e $log_path ]
then
	:
else
	mkdir $log_path
fi  #build log path if not exist^^ NOTE: only works if subdirectories already exists

error_log='error_log.txt'
message_log='message_log.txt'
function logger {
     today=$(date +%Y-%m-%d.%H.%M)
     "$@" "$today"> >(tee -a $log_path$message_log) 2> >(tee -a $log_path$error_log >&2)
}
#=============#=============#=============#=============
#=============#=============#=============#=============
#=============#=============#=============#=============
#=============#=============#=============#=============
#=============#=============#=============#=============
#SETTING LOG CONFIGURATION:
if [[ -e "$log_path$error_log" || -e "$log_path$message_log" ]]
	then 
		logger echo 'message or error log file already exists, overwrite, new or abort?(o/n/a)'
		read number
		if [[ $number == o ]]
		then 
			logger echo "using default file name $error_log"
		elif [[ $number == n ]]
		then 
			logger echo 'enter file name:'
			read number
			error_log=$number
			message_log="message_$number"
			logger echo "user choice log file: $number (o/n/a) overwrite/new/abort"
	elif [[ $number != o && number != a ]]
		then 
			logger echo 'exit code 1'
			exit 1
		fi
fi
#=============#=============#=============#=============
logger echo $(date) "user local IP: $(hostname -I)"
#
###=============#=============#=============#=============
###GET USER INPUT:
logger echo "tomcat or apache(a/t)?(ENTER to confirm)"
read tom_ap_answer
case $tom_ap_answer in
	"a")
	logger echo "you have chosen apache"
	software=httpd
	previous=$(yum list installed | grep httpd);;

	"t")
	logger echo "you have chosen tomcat"
	software=tomcat
	previous=$(yum list installed | grep tomcat);;

	*)
	logger echo "please respond with a or t, aborting..."
	exit 3;;
esac

###=============#=============#=============#=============
###CHECK PRESINSTALLED:
if [ -z "$previous" ]
then
	logger echo "no previous install detected"
else
	echo '================PREVIOUS VERSIONS:'
	logger echo "$previous"
	echo '================STATUS:'
	logger echo "$(systemctl status $software)"
	logger echo "previous install(s) detected, continue? (y/n)"
	read answer
	if [ $answer == y ]
	then
		logger echo 'continuing with install, press crtl + C too abort, note this is an abrupt stop and may cause problems'
	elif [ $answer == n ]
	then
		echo 'aborted'
		logger echo 'exit code 5'
		exit 5
	else
		echo 'erroneous input, aborting'
		logger echo 'exit code 4'
		exit 4
	fi
fi

logger echo $(date) "previous installs (might be empty):$previous"

###=============#=============#=============#=============
###=============#=============#=============#=============
###INSTALLING: tomcat or apache:
logger echo 'installing ...'
sudo yum install "$software"
###DO STATUS CHECK	
echo "==================="
echo "==================="
echo "status check $software:"
systemctl status "$software"
###START software:
systemctl start "$software"
systemctl status "$software"
logger echo "start $software automatically when server starts? (y/n)"
read answer	
if [ $answer == y ]
	then
		systemctl enable "$software"
		logger echo "added $software to startup"

	elif [ $answer == n ]
	then
		logger echo "not adding $software to startup"

	elif [ $answer != n && $answer != y ]
	then
		echo 'incorrect response, nothing added to startup, exit code 4'
		exit 4
fi

		
###=============#=============#=============#=============
###DATA LOGGING:
##
##all works through logger function atm
##
###=============#=============#=============#=============
###CLEANUP
###set a reminder to delete log files + check size and status of installed files
echo '===================='
echo '===================='

logger echo $(date) "current version $software:: $(rpm -qa $software)"
logger echo $(date) "installation finished, log files stored in $log_path--> $error_log, $message_log"

exit 0
