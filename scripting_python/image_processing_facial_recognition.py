#WEBCRAWLER_MAKING_API_CALL
"""
This script crops images based on the faces present. 
-it only handles images with a single face, if two or more faces are reciognized the picture is ignored
-it uses 3 paths: 
	-1 the top directory where soem text files will be stored
	-2 subdirectory for where to look for images: make sure this folder only contains the images to be processed
	-3 subdirectory for where the cropped images will be stored
-there are some parameters that can be altered:
	-1 padding: this increases or decreases the area around the detected face
	-2 grayscale: a boolean that either converts all images to grayscale or leave orignal colors: default is grayscale TRUE
-password for the faceXAPI calls is stored in a separate module called: configuration_SECRET
 !NOTE: as a best practice this class should be compiled and stored away from this main script!
-some general configuration options are in module called: configuration_GENERAL


These resources have been used:
https://facex.io/
https://facex.io/user-guide.html
https://github.com/teamfacex/facex-sample-codes
API documentation:
https://documenter.getpostman.com/view/6700149/S11Lrcuj?version=latest#5997a3ba-fb8a-4695-a4a3-1da9e3cce387
"""

#IMPORTS
import requests
import os
from PIL import Image, ImageOps, ImageDraw
#IMPORT id and key
secret_path = "<HIDDEN>"
os.chdir(secret_path)
from configuration_SECRET  import USER_ID, USER_KEY# this needs to be added to path, ior current path should be where secret is located
print("CURRENT PATH: ", os.getcwd())

stimuli_path = "C:\\RoeRoe\\PYTHON_crop_test_stim\\"
cropped_stimuli_path = "C:\\RoeRoe\\PYTHON_crop_test_stim_cropped\\"
os.chdir(stimuli_path)
assert os.getcwd() == stimuli_path[:-1]
print("CURRENT PATH: ", os.getcwd())


#get names of all images in the subfolder:
file_names = os.listdir()

#################################################################################
##### MAIN BODY #####
##### MAIN BODY #####
##### MAIN BODY #####
#################################################################################

#################################################################################
#	1 making API calls for each image to faceX and getting the bounding box for the face:
#################################################################################
headers = {"user_id": USER_ID ,"user_key": USER_KEY} 
json_list =[]
counter = 0 
for i in file_names: 
	IMAGE_PATH= '%s%s'%(stimuli_path, i)
	open(IMAGE_PATH, 'rb')

	##for image attibute use these lines:
	API_URL = "http://facexapi.com/get_image_attr?face_det=1" # face attribute url
	files = {'image_attr': open(IMAGE_PATH, 'rb')}
	r = requests.post(API_URL,headers = headers,files = files) # comment this line to use url image
	
	##for face vector use these lines:
	#API_URL = "http://facexapi.com/get_face_vec?face_det=1"
	#files = {'img': open(IMAGE_PATH, 'rb')}
	#r = requests.post(API_URL,headers = headers,files = files) # comment this line to use url image
	
	##for URL use these lines:
	#payload = {"image_attr": IMAGE_URL}
	#r = requests.post(url = API_URL,headers = headers,data = payload)
	

	#print(r.text) # printing response: only needed if the data has to be inspected
	
	json_list.append(r.json())
	##filter out non faces  #if fector == Null = filter: make new file list	
	if 'null' in r.text:
		del file_names[counter]
		del json_list[counter]
	counter+=1

print("length of current image list: ", len(json_list))
#################################################################################
###save the filtered list of file names in textfile: (OPTIONAL)
#################################################################################
#os.chdir('%s'%(path))
#filtered_file_names= open('filtered_file_names.txt', 'wt')
#for name in file_names:
#	filtered_file_names.writelines('%s/n'%name)#for L = file_names
#filtered_file_names.close()


#################################################################################
# 	2 	cropping the images based on the bounding box
#################################################################################
"""
this is the bounding box::
json_list[0]['face_id_0']['face_rectangle']

this is the logic of the cropping border:
#border = (0, 165, 0, 77) # left, up, right, bottom: number indicate # of pixels from the indicated side towards center of image
"""
counter = 0 
padding = 0  #make this relative to the overall size of the image # make sure that if crop is zero: pading also zero
greyscale = True

for file in file_names:
	
	myimage = Image.open(file)
	
	#OPTIONAL + DEBUGGING:
	#myimage.show() #
	#print(myimage.size)
	#draw = ImageDraw.Draw(myimage)
	#r = 5 #determines the size of the circle
	#for x in border:
#		y = 50
#		draw.ellipse((x-r, y-r, x+r, y+r), fill=(50+(50*counter),0,0,50+(50*counter)))
#		draw.ellipse((y-r, x-r, y+r, x+r), fill=(50+(50*counter),0,0,50+(50*counter)))
#		counter+=1
#	counter=0
	#x1, y1, x2, y2 =border[0], border[1], border[2], border[3]
	#draw.ellipse((x1-r, y1-r, x1+r, y1+r), fill=(250,0,0,250))
	#draw.ellipse((x2-r, y2-r, x2+r, y2+r), fill=(250,0,0,250))
	#myimage.show()
	
	#skip the image if there are either no faces or 1> faces
	if 'face_id_0' not in json_list[counter].keys() or 'face_id_1' in json_list[counter].keys():
		print(json_list[counter].keys(),'###SKIPPING IMAGE %s' %counter)
		counter+=1
		continue
	
	#translate bounding box to croppped values:
	#REMINDER: border = (0, 165, 0, 77) # left, up, right, bottom
	
	border =json_list[counter]['face_id_0']['face_rectangle']
	
	#check to correct for padding if at the edge of the image:
	#bool_check = padding => border[0]
	
	
	border[0]= border[0] + padding
	border[1]= border[1] + padding
	width = json_list[counter]['image_size'][0]
	height = json_list[counter]['image_size'][1]
	border[2] = (width - json_list[counter]['face_id_0']['face_rectangle'][2]) + padding
	border[3] = (height - json_list[counter]['face_id_0']['face_rectangle'][3]) + padding
	
	#covnert to tuple, this is needed for the Image Ops method
	border=tuple(border)
	cropped=ImageOps.crop(myimage, border)
	
	#convert to grayscale
	if greyscale == True:
		cropped = cropped.convert('LA')
	
	#save the image
	#NOTE: change path to save cropped images in separate folder
	#NOTE this work with jpg, might not work with longer extentions
	cropped.save("%s%s.png" %(cropped_stimuli_path, file[:-4]))
	
	counter +=1
#################################################################################
##todo:
#################################################################################
##essential:
#done    filter out non faces  #if fector == Null = filter: make new file list
#done    convert to grayscale
#done    crop the image based on call ^ (use a class)
#equiluminant
#scale to equal pixel quality
#make sure that file is saved neater than: [:file-4] !!

##nice to have:
#make interface that allows user input for settings: greyscale: path: padding
#finish padding: deal with crop = 0 and padding = 20 = -20 out side of image error
#package into a complete standalone module:: make sure to use classes and objects
#avoid vendor locking and try other API / face recognition services

